//
//  ImageCollectionViewCell.swift
//  PhotoSearch
//
//  Created by Rishi Lund on 08/03/21.
//  Copyright © 2021 Rishi Lund. All rights reserved.
//

import UIKit
import SDWebImage
import IHProgressHUD

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageInCell: UIImageView!
    
    
    
    override func awakeFromNib() {
        //set up code
    }
    
    
    func configureCell(with urlString : String){
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        imageInCell.sd_setImage(with: url,
                                placeholderImage: nil,
                                options: .highPriority) { (downloadedImage, erro, cachType, downloadedURL) in
                                    if let error = erro{
                                        print(error.localizedDescription)
                                    }

        }
        
    }
}

//        URLSession.shared.dataTask(with: url) { (data, _, error) in
//
//            guard let retunredData = data, error == nil else {
//                return
//            }
//            DispatchQueue.main.async {
//                let image = UIImage(data: retunredData)
//                self.imageInCell.image = image
//            }
//        }.resume()

//
//        let task = URLSession.shared.dataTask(with: requestURL) { (data, _, error) in
//
//        }
