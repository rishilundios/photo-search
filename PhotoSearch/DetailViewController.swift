//
//  DetailViewController.swift
//  PhotoSearch
//
//  Created by Rishi Lund on 09/03/21.
//  Copyright © 2021 Rishi Lund. All rights reserved.
//

import UIKit
import SDWebImage
import IHProgressHUD

class DetailViewController: UIViewController {

    @IBOutlet weak var detailLBL: UILabel!
    @IBOutlet weak var imageViewDetail: UIImageView!
    
    var imageViewString : String?
    var labelString : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpIHProgressHUD()
        IHProgressHUD.show(withStatus: "Loading...")
        
        if let text = labelString{
            detailLBL.text = "\(String(describing: text.first!.uppercased()))\(text.dropFirst())"
        } else {
            
        }
        
        if let imageString = imageViewString{
            
            setImageView(with: imageString)
        } else {}
        
    }
    
    func setUpIHProgressHUD(){
        IHProgressHUD.set(defaultMaskType: .black)
        IHProgressHUD.set(foregroundColor: .black)
        IHProgressHUD.set(ringThickness: 4.0)
        IHProgressHUD.set(backgroundColor: .lightGray)
    }
    
    func setImageView(with urlString : String){
        
        DispatchQueue.global(qos: .default).async(execute: {
            
            guard let url = URL(string: urlString) else {
                return
            }
            
            URLSession.shared.dataTask(with: url) { [weak self] (returnedData, _, error) in
                guard let data = returnedData , error == nil else {
                    return
                }
                DispatchQueue.main.async {
                    let image = UIImage(data: data)
                    self!.imageViewDetail.image = image
                    IHProgressHUD.dismiss()
                }
                }.resume()
            
            
        })
        
       
        
    }
}
