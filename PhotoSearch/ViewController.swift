//
//  ViewController.swift
//  PhotoSearch
//
//  Created by Rishi Lund on 07/03/21.
//  Copyright © 2021 Rishi Lund. All rights reserved.
//

import UIKit
import Reachability
import IHProgressHUD

struct Result : Codable {
    var id : String?
    var urls : URLS?
    var alt_description : String?
}
struct URLS : Codable {
    var thumb : String?
    var full : String?
}

struct APIResponse : Codable {
    var total : Int?
    var total_pages : Int?
    var results : [Result]?
}


class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionViewPhoto: UICollectionView!
    
    var results : [Result] = []
    var imageDetailView : UIImageView?
    var smallView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpIHProgressHUD()
        checkReachability()
        
    }
    
    func setUpIHProgressHUD(){
        IHProgressHUD.set(defaultMaskType: .black)
        IHProgressHUD.set(foregroundColor: .black)
        IHProgressHUD.set(ringThickness: 4.0)
        IHProgressHUD.set(backgroundColor: .lightGray)
    }

    func fetchPhotos(searchQuery : String){
        
        let urlString = "https://api.unsplash.com/search/photos?page=2&per_page=200&order_by=default&query=\(searchQuery)&client_id=HTOuLphnLgY4XyhT9orNi1pW7oZblFZRqZdcJWofdU0"
        
        //added for checking BitBucket
        

        
        guard let url = URL(string: urlString) else {
            return
        }
        

        let task = URLSession.shared.dataTask(with: url) {[weak self] (data, _, error) in
            guard let returnedData = data, error == nil else{
               // self?.collectionViewPhoto.isHidden = true
                self?.showNoDataView()
                return
                }
            
            do {
                let jsonResults = try JSONDecoder().decode(APIResponse.self, from: returnedData)
                print(jsonResults.total)
                DispatchQueue.main.async {
                    self?.results = jsonResults.results!
                    self?.collectionViewPhoto.reloadData()
                    IHProgressHUD.dismiss()
                }
                
            } catch {
                print(error.localizedDescription)
               // self?.showAlert(message: error.localizedDescription)
            }
        }
        task.resume()
    }

    
}

extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let imageURLString = results[indexPath.row].urls?.thumb
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImageCollectionViewCell
        cell.backgroundColor = .yellow
        cell.configureCell(with: imageURLString!)
        return cell
    }
}

extension ViewController{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let imageURLString = results[indexPath.row].urls?.full
        print(imageURLString)
        print(results[indexPath.row].alt_description)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailViewController
        vc.imageViewString = imageURLString
        
        vc.labelString = results[indexPath.row].alt_description
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension ViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / 2) - 2, height: (collectionView.frame.size.height / 3) - 2)
    }
}



extension ViewController : UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            results = []
            collectionViewPhoto.reloadData()
            fetchPhotos(searchQuery: text)
            IHProgressHUD.show(withStatus: "Loading...")
        }
    }
}

extension ViewController{
    func checkReachability(){
        let reachability = try! Reachability()
        
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
}

extension ViewController{
    func showNoDataView(){
        smallView = UIView(frame: CGRect(x: self.view.frame.origin.x, y: 150, width: self.view.frame.width, height: 250))
        smallView.isHidden = false
        self.view.addSubview(smallView)
    }
    
    func hideNoDataView(){
        smallView.isHidden = true
        self.collectionViewPhoto.isHidden = false
    }
    
    func showAlert(message : String){
        IHProgressHUD.dismiss()
  
        let alert = UIAlertController(title: "No data search for 'Car'", message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        alert.addAction(ok)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
        
    }
}
